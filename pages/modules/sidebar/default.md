---
routable: false
---

#### About

Когда-нибудь я стану ~~хокаге~~ самым лучшим системным программистом!

А пока места для выпендрежа:
* [GitHub](https://github.com/AnyKeyShik)
* [GitLab](https://gitlab.com/AnyKeyShik)

И контакты:
* [Telegram](https://t.me/AnyKeyShik)
* [E-mail](mailto:admin@anykeyshik.xyz)


